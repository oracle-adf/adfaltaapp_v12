package com.redsamurai.view.beans;


public class SkinSelector {
    private String currentSkin;
    
    public SkinSelector() {
        super();
    }
    
    public String getCurrentSkin() {
        if (this.currentSkin == null) {
            this.currentSkin = "WorkBetter";
        }
        return this.currentSkin;
    }
    
    public void setCurrentSkin(String currentSkin) {
        this.currentSkin = currentSkin;
    }
}
