var wsUri = "ws://";
var socketendpoint = "/WebSocket/service";
var websocket;

function getWSUri() {
  var url = window.location.href;
  var arr = url.split("/");
  url= wsUri + arr[2] + socketendpoint;
  return url;
}

function connectSocket() {  
  if ('WebSocket' in window){
    websocket = new WebSocket(getWSUri());    
    websocket.onmessage = onMessage;   
    websocket.onerror = onError;
    websocket.onclose = onClose;
    console.log('socket opened !');
  } else {
    console.log('websocket not supported...!')
  }
}

//on error event
function onError(evt) {
  console.log('error :' + evt);
}

//on close event
function onClose(evt) {
  console.log('websocket closed :' + evt.code + ":" + evt.reason);
  
  closeSession();
}

function onMessage(evt) {
  var msg = JSON.parse(evt.data);
  var friendsCount = msg.friendsCount;
  var followersCount = msg.followersCount;
  var text = msg.text;
  var userName = msg.userName;
  var userLocation = msg.userLocation;
  var userLanguage = msg.userLanguage;
  
  var friendsCountUI = AdfPage.PAGE.findComponentByAbsoluteId("pt1:cBodFDC:r8:0:inft0:ot3");
  friendsCountUI.setValue(friendsCount);
  var followersCountUI = AdfPage.PAGE.findComponentByAbsoluteId("pt1:cBodFDC:r8:0:inft0:ot8");
  followersCountUI.setValue(followersCount);
  var textUI = AdfPage.PAGE.findComponentByAbsoluteId("pt1:cBodFDC:r8:0:ot2");
  textUI.setValue(text);
  var userNameUI = AdfPage.PAGE.findComponentByAbsoluteId("pt1:cBodFDC:r8:0:ot4");
  userNameUI.setValue(userName);
  var userLocationUI = AdfPage.PAGE.findComponentByAbsoluteId("pt1:cBodFDC:r8:0:ot7");
  userLocationUI.setValue(userLocation);
  var userLanguageUI = AdfPage.PAGE.findComponentByAbsoluteId("pt1:cBodFDC:r8:0:ot9");
  userLanguageUI.setValue(userLanguage);
  var friendsCountMeterUI = AdfPage.PAGE.findComponentByAbsoluteId("pt1:cBodFDC:r8:0:statusMeterGauge4");
  friendsCountMeterUI.setValue(friendsCount);
  var followersCountMeterUI = AdfPage.PAGE.findComponentByAbsoluteId("pt1:cBodFDC:r8:0:statusMeterGauge3");
  followersCountMeterUI.setValue(followersCount);
  var friendsCountHorizontalUI = AdfPage.PAGE.findComponentByAbsoluteId("pt1:cBodFDC:r8:0:statusMeterGauge6");
  friendsCountHorizontalUI.setValue(friendsCount);
  var followersCountHorizonatlUI = AdfPage.PAGE.findComponentByAbsoluteId("pt1:cBodFDC:r8:0:statusMeterGauge5");
  followersCountHorizonatlUI.setValue(followersCount);
   
  //var params = {webSocketData:evt.data};
  //var element = AdfPage.PAGE.findComponentByAbsoluteId("pt1:cBodFDC:r8:0:b1");
  //AdfCustomEvent.queue(element, "jsWebSocketCall", params, true);
}